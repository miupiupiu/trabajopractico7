package ar.edu.unju.fi.tp7.servicesTest;

import ar.edu.unju.fi.tp7.entity.Libro;
import ar.edu.unju.fi.tp7.exception.LibroException;
import ar.edu.unju.fi.tp7.service.LibroService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
/**
 * Antes de correr los test, es importante ejecutar el script import.sql
 * se encuentra en src/main/resources
 */

@SpringBootTest
public class LibroServiceTest {

    @Autowired
    LibroService libroService;

    static Libro libro1;
    @BeforeAll
    static void setUp() {
        libro1 = new Libro();
        libro1.setAutor("Nana");
        libro1.setIsbn("978-1-56619-909-5");
        libro1.setTitulo("El arte del Kung Fu");
        libro1.setCantDisponible(7);
    }

    @AfterAll
    static void  tearDown(){
        libro1 = null;
    }

    @Test
    void agregarLibro(){
        Libro libroGuardado = libroService.guardarLibro(libro1);
        Libro libroEncontrado = libroService.buscarLibroPorId(libroGuardado.getId());
        assertEquals(libroEncontrado.getId(), libroGuardado.getId());
        assertEquals(libroEncontrado.getCantDisponible(), libroGuardado.getCantDisponible());
        assertEquals(libroEncontrado.getAutor(), libroGuardado.getAutor());
    }
    @Test
    void editarLibro() throws LibroException {
        libro1.setTitulo("Las gallinas lloronas");
        libro1.setId(11);
        Libro libroEditado= libroService.editarLibro(libro1);
        assertEquals(11,libroEditado.getId());
        assertEquals("Las gallinas lloronas",libroEditado.getTitulo());
    }
    @Test
    void eliminarLibro() throws LibroException {
        libroService.eliminarLibro(3);
        assertThrows(NoSuchElementException.class,()->libroService.buscarLibroPorId(6));
    }
    @Test
    void buscarPorAutor(){
        List<Libro> resultado= libroService.buscarPorAutor("Pandy");
        for (Libro libro: resultado){
            assertEquals("Pandy",libro.getAutor());
        }
    }
    @Test
    void buscarPorTitulo(){
        List<Libro> resultado= libroService.buscarPorTitulo("Mis memorias");
        for (Libro libro: resultado){
            assertEquals("Mis memorias",libro.getTitulo());
        }
    }
    @Test
    void buscarPorIsbn() throws LibroException {
        Libro libroResultado= libroService.buscarPorIsbn("978-1-56619-909-2");
        assertEquals("978-1-56619-909-2",libroResultado.getIsbn());
    }
}
