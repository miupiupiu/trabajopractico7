package ar.edu.unju.fi.tp7.servicesTest;

import ar.edu.unju.fi.tp7.entity.Miembro;
import ar.edu.unju.fi.tp7.exception.LibroException;
import ar.edu.unju.fi.tp7.service.MiembroService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Antes de correr los test, es importante ejecutar el script import.sql
 * se encuentra en src/main/resources
 */

@SpringBootTest
public class MiembroServiceTest {
    @Autowired
    MiembroService miembroService;

    static Miembro miembro1;

    @BeforeAll
    static void setUp(){
        miembro1 = new Miembro();
        miembro1.setNumMiembro(60);
        miembro1.setEmail("Laura@email.com");
        miembro1.setNombre("Laura");
        miembro1.setTelefono("3886784963");

    }

    @AfterAll
    static void tearDown(){
        miembro1 =null;
    }

    @Test
    void guardarMiembro(){
        Miembro  miembroGuardado = miembroService.guardarMiembro(miembro1);
        Miembro miembroEncontrado = miembroService.buscarMiembroPorId(miembroGuardado.getId());
        assertEquals(miembroGuardado.getId(),miembroEncontrado.getId());
        assertEquals(miembroGuardado.getNumMiembro(),miembroEncontrado.getNumMiembro());
    }

    @Test
    void editarMiembro() throws LibroException {
        miembro1.setNombre("Laurita");
        miembro1.setId(12);
        Miembro miembroEditado = miembroService.editarMiembro(miembro1);
        assertEquals(12,miembroEditado.getId());
        assertEquals("Laurita",miembroEditado.getNombre());
    }

    @Test
    void eliminarMiembro() throws LibroException {
        miembroService.eliminarMiembro(4);
        assertThrows(NoSuchElementException.class,()->miembroService.buscarMiembroPorId(4));
    }

}
