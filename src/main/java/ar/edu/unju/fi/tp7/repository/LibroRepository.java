package ar.edu.unju.fi.tp7.repository;

import ar.edu.unju.fi.tp7.entity.Libro;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LibroRepository extends CrudRepository<Libro, Integer> {
    List<Libro> findByTitulo(String titulo);

    List<Libro> findByAutor(String autor);
    Optional<Libro> findByIsbn(String isbn);

}
