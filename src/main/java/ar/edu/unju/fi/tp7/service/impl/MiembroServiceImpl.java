package ar.edu.unju.fi.tp7.service.impl;

import ar.edu.unju.fi.tp7.entity.Miembro;
import ar.edu.unju.fi.tp7.exception.LibroException;
import ar.edu.unju.fi.tp7.repository.MiembroRepositary;
import ar.edu.unju.fi.tp7.service.MiembroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MiembroServiceImpl implements MiembroService {
    private MiembroRepositary miembroRepositary;

    @Autowired
    public MiembroServiceImpl(MiembroRepositary miembroRepositary) {
        this.miembroRepositary = miembroRepositary;
    }

    @Override
    public Miembro guardarMiembro(Miembro miembro) {
        return this.miembroRepositary.save(miembro);
    }

    @Override
    public Miembro editarMiembro(Miembro miembro) throws LibroException {
        if (miembroRepositary.existsById(miembro.getId())){
            return miembroRepositary.save(miembro);
        }
        else{
            throw new LibroException("El miembro con id:"+ miembro.getId() + "no existe");
        }
    }

    @Override
    public void eliminarMiembro(Integer id) throws LibroException {
        if (miembroRepositary.existsById(id)){
            miembroRepositary.deleteById(id);
        }
        else{
            throw new LibroException("El miembro con id:"+ id + "no existe");
        }
    }

    @Override
    public Miembro buscarMiembroPorId(Integer id) {
        return miembroRepositary.findById(id).orElseThrow();
    }


}
