package ar.edu.unju.fi.tp7.exception;

public class LibroException extends Exception{
    public LibroException(String message){
        super(message);
    }
}
