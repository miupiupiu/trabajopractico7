package ar.edu.unju.fi.tp7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrabajoPractico7Application {

	public static void main(String[] args) {
		SpringApplication.run(TrabajoPractico7Application.class, args);
	}

}
