package ar.edu.unju.fi.tp7.repository;

import ar.edu.unju.fi.tp7.entity.Miembro;
import org.springframework.data.repository.CrudRepository;

public interface MiembroRepositary extends CrudRepository<Miembro, Integer> {

}
