package ar.edu.unju.fi.tp7.entity;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "miembro")
public class Miembro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;

    @Column(name = "num_miembro")
    private int numMiembro;
    private String email;
    private String telefono;


    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy = "miembro")
    private List<Prestamo> prestamos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumMiembro() {
        return numMiembro;
    }

    public void setNumMiembro(int numMiembro) {
        this.numMiembro = numMiembro;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<Prestamo> getPrestamos() {
        return prestamos;
    }

    public void setPrestamos(List<Prestamo> prestamos) {
        this.prestamos = prestamos;
    }
}
