package ar.edu.unju.fi.tp7.service.impl;

import ar.edu.unju.fi.tp7.entity.Libro;
import ar.edu.unju.fi.tp7.exception.LibroException;
import ar.edu.unju.fi.tp7.repository.LibroRepository;
import ar.edu.unju.fi.tp7.service.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LibroServiceImpl implements LibroService {

    private LibroRepository libroRepository;

    @Autowired
    public LibroServiceImpl(LibroRepository libroRepository) {
        this.libroRepository = libroRepository;
    }

    @Override
    public Libro guardarLibro(Libro libro) {
        return libroRepository.save(libro);
    }

    @Override
    public Libro buscarLibroPorId(Integer id) {
        return libroRepository.findById(id).orElseThrow();
    }

    @Override
    public Libro editarLibro(Libro libro) throws LibroException{
       if(libroRepository.existsById(libro.getId())){
           return libroRepository.save(libro);
       }
       else {
           throw new LibroException("El id: " + libro.getId() + "no existe");
       }
    }

    @Override
    public void eliminarLibro(Integer id) throws LibroException {
        if(libroRepository.existsById(id)){
            libroRepository.deleteById(id);
        }
        else {
            throw new LibroException("El id: " + id + "no existe");
        }
    }

    @Override
    public List<Libro> buscarPorTitulo(String titulo) {
        return libroRepository.findByTitulo(titulo);
    }

    @Override
    public List<Libro> buscarPorAutor(String autor) {
        return libroRepository.findByAutor(autor);
    }

    @Override
    public Libro buscarPorIsbn(String isbn) throws LibroException {
        Optional<Libro> libroAux= libroRepository.findByIsbn(isbn);
        if(libroAux.isPresent()){
            return libroAux.get();
        }
        else{
            throw new LibroException("El isbn buscado no existe");
        }
    }

}
