package ar.edu.unju.fi.tp7.service;

import ar.edu.unju.fi.tp7.entity.Libro;
import ar.edu.unju.fi.tp7.exception.LibroException;

import java.util.List;

public interface LibroService {
    /**
     * Guarda un libro en la base de datos
     * @param libro
     * @return el libro incluyendo su clave primaria
     */
    Libro guardarLibro(Libro libro);

    /**
     * Busca un libro por id
     * @param id
     * @return el libro si existe o NoSuchElementException
     */
    Libro buscarLibroPorId(Integer id);

    /**
     * Edita un libro
     * @param libro
     * @return el libro editado
     * @throws LibroException
     */

    Libro editarLibro(Libro libro) throws LibroException;

    /**
     * Elimina un libro por id
     * @param id
     * @throws LibroException
     */
    void eliminarLibro(Integer id) throws LibroException;

    /**
     * Busca libros que coincidan por el titulo
     * @param titulo
     * @return lista de libros
     */

    List<Libro> buscarPorTitulo(String titulo);

    /**
     * Busca libros que coincidan por el autor
     * @param autor
     * @return lista de libros
     */
    List<Libro> buscarPorAutor(String autor);

    /**
     * Busca libros que coincidan por el isbn
     * @param isbn
     * @return libro
     * @throws LibroException
     */
    Libro buscarPorIsbn(String isbn) throws LibroException;

}
