package ar.edu.unju.fi.tp7.service;

import ar.edu.unju.fi.tp7.entity.Miembro;
import ar.edu.unju.fi.tp7.exception.LibroException;


public interface MiembroService {
    /**
     * Guarda un miembro
     * @param miembro
     * @return miembro
     */
    Miembro guardarMiembro(Miembro miembro);

    /**
     * Edita un miembro
     * @param miembro
     * @return miembro
     * @throws LibroException
     */
    Miembro editarMiembro(Miembro miembro) throws LibroException;

    /**
     * Elimina un miembro por id
     * @param id
     * @throws LibroException
     */
    void eliminarMiembro(Integer id)throws LibroException;

    /**
     * Buscar meimbro por id
     * @param id
     * @return miembro
     */
    Miembro buscarMiembroPorId(Integer id);
}
