package ar.edu.unju.fi.tp7.repository;

import ar.edu.unju.fi.tp7.entity.Prestamo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrestamoRepositary extends CrudRepository<Prestamo,Integer> {

}
