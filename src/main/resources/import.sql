insert into libro (cant_disponible, autor, isbn, titulo) values (10, "Nana", "978-1-56619-909-4", "Como comer maiz en 3 simples pasos");
insert into libro (cant_disponible, autor, isbn, titulo) values (1, "Pandy", "978-1-56619-909-3", "Recuerdos de primavera");
insert into libro (cant_disponible, autor, isbn, titulo) values (20, "Nana", "978-1-56619-909-2", "Plantas de estacion");
insert into libro (cant_disponible, autor, isbn, titulo) values (3, "Desconocido", "978-1-56619-909-1", "Mis memorias");
insert into libro (cant_disponible, autor, isbn, titulo) values (5, "Nana", "978-1-56619-909-0", "Recetas con ciruelas");

insert into miembro(num_miembro, email, nombre, telefono) values (15,"juan@email.com","Juan","3885485126");
insert into miembro(num_miembro, email, nombre, telefono) values (26,"susana@email.com","Susana","3884859658");
insert into miembro(num_miembro, email, nombre, telefono) values (37,"roxana@email.com","Roxana","3886781964");
insert into miembro(num_miembro, email, nombre, telefono) values (48,"pedro@email.com","Pedro","3885478295");
insert into miembro(num_miembro, email, nombre, telefono) values (59,"Dexter@email.com","Dexter","3884785149");